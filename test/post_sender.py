'''
Ta skripta pošlje vsakega od setov podatkov v json obliki iz mape ./subjects
kot POST request serverju in izpiše dobljene rezultate.
'''
import requests
import json
import os
import time

destination = 'http://0.0.0.0:8080'
api_key = '12345678'
path = 'subjects/'

start_time = time.time()
for json_file in os.listdir(path):
    with open(path + json_file, 'r') as f:
        data_to_send = json.load(f)

    print('Sending data')
    try:
        r = requests.post(url=destination, json=data_to_send, headers={'api_key': api_key})
        print('Data sent')
    except:
        print('Failed')

    print(f'Recived data: {r.text}')

print(f'Completed in {time.time() - start_time}s')