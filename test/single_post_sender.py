'''
Ta skripta pošlje set podatkov v json obliki kot POST request serverju
in izpiše dobljen rezultat.
'''
import requests
import json

destination = 'http://0.0.0.0:8080'
api_key = '12345678'
path = 'subjects/671301001.json'

with open(path , 'r') as f:
    data_to_send = json.load(f)

print('Sending data')
try:
    r = requests.post(url=destination, json=data_to_send, headers={'api_key': api_key})
    print('Data sent')
except:
    print('ERROR: sending failed')

print(f'Recived data: {r.text}')