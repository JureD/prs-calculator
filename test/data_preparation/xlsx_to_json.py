import numpy as np
import pandas as pd
import json

# Uvoz podatkov:
podatki = pd.read_excel('./Supplementary_Table_genotype_data.xlsx', header=1, skiprows=0)
# Formatiranje NaN poadtkov
podatki_brez_nan = podatki.fillna(method='ffill').fillna(method='bfill')
# Uvoz podatkov za uteži
utezi = pd.read_excel('./Supplementary_Table_3.xlsx', header=1, usecols='A,F,G')
utezi = utezi.transpose()
utezi.columns = utezi.iloc[0]

for i in range(5599):
    subject_id = podatki_brez_nan.iloc[i][0]
    phenoroup = podatki_brez_nan.iloc[i][1]
    age = podatki_brez_nan.iloc[i][2]
    gender = podatki_brez_nan.iloc[i][3]
    country = podatki_brez_nan.iloc[i][4]
    snps = []
    for snp in podatki_brez_nan.columns[6:]:
        snps.append(snp)

    genotype = {}
    for snp in snps:
        genotype[snp] = podatki_brez_nan[snp][i]

    with open(f'subjects/{subject_id}.json', 'x') as f:
        json_data = {'id': subject_id, 'phenogroup': phenoroup, 'age': age, 'gender': gender, 'country': country, 'genotype': genotype}
        json.dump(json_data, f)

utezi_json_data = {}
for utez in utezi:
    slovar_utezi = {}
    slovar_utezi['referencni_alel'] = utezi[utez][2]
    slovar_utezi['utez'] = utezi[utez][1]
    utezi_json_data[utezi[utez][0]] = slovar_utezi

with open('utezi.json', 'x') as f:
    json.dump(utezi_json_data, f)