import json
import time

class Subject():
    '''
    Class for manipulation of sujects data
    and calculation of sujects PRS.
    '''
    def __init__(self, data, weights):
        '''
        Initialize Subject object

        Parameters:
        data (json-stringified): SNP data of subject
        weights (json): weights for SNP refernece alleles
        '''
        self.data = data
        self.id = data['id']
        self.phenogroup = data['phenogroup']
        self.age = data['age']
        self.gender = data['gender']
        self.country = data['country']
        self.genotype = data['genotype']

        with open(weights, 'r') as f:
            self.weights = json.load(f)
    
    def __repr__(self):
        '''
        Representation method of Subject object
        '''
        return f'id: {self.id}'

    def write_data(self):
        '''
        Write SNP data to json file.
        (For development purposes)
        '''
        with open(f'subject_{self.id}.json', 'x') as f:
            json.dump(self.data, f)
    
    def __calculate_prs(self):
        '''
        Calculate PRS from SNP data.

        Returns:
        float:PRScore
        '''
        self.prs = 0
        start_time = time.time()
        for snp in self.genotype:
            if (2 * self.weights[snp]['referencni_alel'] in self.genotype[snp]):
                self.prs += 2 * self.weights[snp]['utez']
            elif self.weights[snp]['referencni_alel'] in self.genotype[snp]:
                self.prs += self.weights[snp]['utez']
            else:
                pass
        self.calculation_time = time.time() - start_time
        return self.prs

    def log_prs(self, location):
        '''
        Write PRS to logging file.

        Parameters:
        location (str): location of logging file
        '''
        self.__calculate_prs()
        with open(location, 'a') as f:
            f.write(f'{self.id}:\t{self.prs}\n')

