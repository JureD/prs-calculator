from flask import Flask, jsonify, request
from helper import Subject
import os

# Load API key from environment variable
api_key = os.environ.get('API_KEY')
# Loation of json file containing SNP weights
weights_file = './static/weights_data/utezi.json'
# Loation of logging file
log_file = './static/prs_logs/prs_log.txt'

# Init Flask
app = Flask(__name__)

# Route for hostname:port/ (only accepts POST requests)
@app.route('/', methods=['POST'])
def receive_data():
    # Chack if incomming request is POST
    if (request.method == 'POST'):
        # Chack if API keys match
        auth = request.headers.get('api_key')
        if auth == api_key:
            # Get recieved json 
            recieved_data = request.get_json()
            # Init instance of Subject class
            subject = Subject(recieved_data, weights_file)
            # Call method log_prs of subject object
            subject.log_prs(log_file)
            # Return json with resoult ot failure data
            return jsonify({'status': 'FINISHED', 'subject': subject.id, 'PRS': subject.prs, 'calculation time': subject.calculation_time}), 200
        else:
            # Return json with rejection data
            return jsonify({'status': 'ERROR: Unauthorized'}), 401
    else:
        pass

# Run app on local machines IP
if __name__ == '__main__':
    app.run(host='0.0.0.0')