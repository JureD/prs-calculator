# PRS calculator
PRS calculator je kontejnerizirana aplikacija, ki sprejme *http POST request*, z podatki o genotipu osebe, v json obliki na TCP portu 8080.

Iz podatkov izračuna PRS za to osebo po metodi opisani v [članku](https://academic.oup.com/hmg/article/27/23/4145/5061268), ga zapiše v *logging file* in ga v json obliki vrne pošiljatelju.

## Vzorčni podatki
Vzorčni podatki so v json format pretvorjeni podatki raziskave MelaNostrum. Nahajajo se v `test/subjects`.

## Shema aplikacije
![Shema PRS calculator aplikacije](/images/flow_diagram.png)

## Building command
Za izradnjo docker image:

```bash
docker build . -t prs_calculator:<version>
```

## Running command
```bash
docker run \
    -p 8080:8080 \
    -e API_KEY=12345678 \
    -v /home/jure/Documents/GenePlanet/snp_processor/test/docker-volume:/prs_calculator/static/prs_logs \
    prs_calculator:<version>
```

## Verzije
V container registry tega repozitorija (`registry.gitlab.com/jured/prs-calculator`) sta dva image-a:
* `prs-calculator:alpine` lažja verzija temelji na alpine linux (~230MB)
* `prs-calculator:ubuntu` normalna verzija temelji na ubuntu (~485MB)