# Normalni image (~485MB):
# Uvoz base-image Ubuntu:latest
#FROM ubuntu

# Posodobitev sistema in install potrebnih paketov:
#RUN apt-get update && \
#apt-get upgrade -y && \
#apt-get install -y python3 python3-pip

# Manjši image (~230MB):
# Uvoz base-image Alpine:latest
FROM alpine

# Posodobitev sistema in install potrebnih paketov:
RUN apk update && \
apk upgrade && \
apk add python3 python3-dev gcc musl-dev && \
pip3 install --upgrade pip

# Kopiranje potrebnih datotek v docker image:
COPY prs_calculator /prs_calculator

# Definicija delovnega direktorija:
WORKDIR /prs_calculator

# Install potrebnih python paketov:
RUN pip3 install -r /prs_calculator/requirements.txt

# Odpiranje porta 8080 za komunikacijo:
EXPOSE 8080

# Zagon aplikacije:
# 1 worker
CMD ["gunicorn", "-b", "0.0.0.0:8080", "wsgi:app"]

# 3 workers (2x CPU-cores +1) an option for more concurrent requests [9]
#CMD ["gunicorn", "-b", "0.0.0.0:8080", "-w", "9", "wsgi:app"]